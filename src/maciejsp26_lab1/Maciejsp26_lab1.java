/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package maciejsp26_lab1;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Maciej
 */
public final class Maciejsp26_lab1 
{
    
    public static final String DRIVER="org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL="jdbc:derby:B:\\Studia\\STUDIA lll ROK\\Semestr 6\\Technologie JavaEE\\Homework\\L1\\maciejsp26_lab1\\maciejsp26_db";
    public static final String QUERY="select * from clothes";
    private static java.sql.Connection connection;
    private Maciejsp26_lab1() { }
    
     public static boolean Connect() throws ClassNotFoundException, SQLException 
    {
        connection = DriverManager.getConnection(JDBC_URL);
        if(connection == null) 
        {
                return false;        
        } 
        else 
        {
                return true;        
        }
    }
    
    public static boolean Disconnect() throws SQLException      
    {
        if (connection == null)
        {
            return false;        
        } 
        else 
        {            
            connection.close();
            return true;        
        }    
    }
    public static String getData() throws SQLException 
    {
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(QUERY);
        ResultSetMetaData rsmd = rs.getMetaData();
        String wiersz = new String();
        int colCount = rsmd.getColumnCount();
        
        for (int i = 1; i <= colCount; i++) 
        {
            wiersz = wiersz.concat(rsmd.getColumnName(i) + " \t| ");        
        }        
        wiersz = wiersz.concat("\r\n");
        while (rs.next()) 
        {
            System.out.println("");
            for (int i = 1; i <= colCount; i++) 
            {                
                wiersz = wiersz.concat(rs.getString(i) + " \t| ");
            }            
            wiersz = wiersz.concat("\r\n");        
        }
        if (stat != null) 
        { 
            stat.close();  
        }  
        return wiersz;    
    }
}
